<?php

function demain($day){
    $tomorrow = new datetime($day);
    $tomorrow->modify('+1 days')->format('Y-m-d');
    $demain = date($tomorrow->format('Y-m-d'));
    return $demain;
}

function quelJour($lundi,$numJour){
	$jour=$lundi;
	for ($i=2; $i <= $numJour; $i++) { 
		$jour=demain($jour);
	}
	return $jour;
}

function semaineProchaine($lundi){
	$d=$lundi;
	for ($i=1; $i <= 7; $i++) { 
		$d=demain($d);
	}
	return $d;
}

