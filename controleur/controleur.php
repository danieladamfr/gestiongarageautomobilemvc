<?php
    
//CONTROLEUR
	require_once('modele/modele.php');
	require_once('vue/vue.php');
	
//AFFICHAGE DIRECTEUR
    function ctlAffichageDirecteur(){
		affichageDirecteur();
	}
//AFFICHAGE ERREUR
	function ctlErreur($erreur){
        $ligne = employePresent();
        $catEmp = $ligne['0'];
        if($catEmp = "Agent") {
            affichageErreurAgent($erreur);
        } elseif ($catEmp = "Directeur"){
            affichageErreurDirecteur($erreur);
        } elseif ($catEmp = "Mecanicien"){
            affichageErreurMecanicien($erreur);
        }
	}
   	
//----------------------------------GESTION DES EMPLOYES------------------------------------------
    //AFFICHAGE EMPLOYE
	function ctlAffichageEmploye() {
		$employes = rechercherLesEmployes();
        $directeur = employePresent();
		affichageEmploye($employes,$directeur);
	}
	//CREER EMPLOYE
	function ctlCreerEmploye($nomEmp,$loginEmp, $mdpEmp, $catEmp) {
		if (!empty($nomEmp) && !empty($loginEmp) && !empty($mdpEmp) && !empty($catEmp)) {
			if((rechercherUnEmploye($nomEmp) == NULL) && rechercherLoginEmploye($loginEmp) == NULL) {               
                creerEmploye($nomEmp,$loginEmp, $mdpEmp, $catEmp);
                ctlAffichageEmploye();
             } else {
                throw new Exception("Cet employé est déjà existant");
               
            }
                
		} else {
			throw new Exception("Un des champs est invalide");
		}
	}
	//SUPPRIMER EMPLOYE
	function ctlSupprimerEmploye(){
		foreach($_POST['check'] as $key => $val) {
			supprimerEmploye($val);
		}
		ctlAffichageEmploye();
	}	
	//MODIFIER EMPLOYE
	function ctlAffichageModifierEmploye($nomEmp){
		if (!empty($nomEmp)) {
			$ligne = rechercherUnEmploye($nomEmp);
			$loginEmp=$ligne->login;
			$mdpEmp=$ligne->mdp;
			$catEmp=$ligne->categorie;
			affichageModifEmp($loginEmp, $mdpEmp , $catEmp, $nomEmp);
		} else {
			throw new Exception("Un des champs est invalide");
		}
			
	}
	function ctlModifierEmploye($loginEmp, $mdpEmp, $catEmp, $nomEmp) {
		if(!empty($loginEmp) && !empty($mdpEmp) &&!empty($catEmp) && !empty($nomEmp)) {
            $employes=rechercherUnEmploye($nomEmp);
            if((rechercherLoginEmploye($loginEmp) == NULL) || ((getLoginEmploye($loginEmp)==1) && ($employes->login==$loginEmp) )){
			modifierEmploye($loginEmp, $mdpEmp, $catEmp,$nomEmp);
			ctlAffichageEmploye();
        } else {
                throw new Exception("Cet identifiant est déjà existant");
            }
		} else {
			throw new Exception("Un des champs est invalide");
		}
	}
	
//----------------------------------GESTION DES INTERVENTIONS--------------------------------------
	//AFFICHAGE INTERVENTION
	function ctlAffichageIntervention(){
        $interventions = rechercherLesInterventions();
        affichageIntervention($interventions);
    }
    //CREER INTERVENTION
	function ctlCreerIntervention($nomTI,$montant,$listePiece){
        if(!empty($nomTI)&&!empty($montant)&&!empty($listePiece)) {
            if(rechercherUneIntervention($nomTI) == NULL) {
                creerIntervention($nomTI,$montant,$listePiece);
                ctlAffichageIntervention();
            } else {
                throw new Exception("Cette intervention est déjà existante");
            }
        } else {
			throw new Exception("Un des champs est invalide");
		}
	}
    //SUPPRIMER INTERVENTION
	function ctlSupprimerIntervention(){
        foreach($_POST['check'] as $key => $val) {
			supprimerIntervention($val);
		}
		ctlAffichageIntervention();
    }	
    //MODIFIER INTERVENTION
    function ctlAffichageModifierIntervention($nomTI) {
        if(!empty($nomTI)) {
            $ligne = rechercherUneIntervention($nomTI);
            $montant=$ligne->montant;
            $listePiece=$ligne->listePiece;
           
            affichageModifTI($montant, $listePiece, $nomTI);
            
       } else {
			throw new Exception("Un des champs est invalide");
		}
			
	}
	function ctlModifierIntervention($nomTI,$montant,$listePiece){
        if(!empty($montant)&& !empty($listePiece)&& !empty($nomTI)) {
            modifierIntervention($montant, $listePiece, $nomTI);
            ctlAffichageIntervention();
       } else {
			throw new Exception("Un des champs est invalide");
		}
	}

//----------------------------------GESTION DES CONNEXIONS------------------------------------------
    //CONNEXION
	function ctlTantativeConnexion($login,$mdp){
        if(!empty($login)&&!empty($mdp)) {
            if(tantativeConnexion($login,$mdp) != false ) {
                if(tantativeConnexion($login,$mdp) != NULL ){
                    $ligne = tantativeConnexion($login,$mdp);
                    $catEmp = $ligne->categorie;
                    $login = $ligne->login;
                    $mdp = $ligne->mdp;
                    $nomEmp = $ligne->nomEmp;
                    ctlInsertionConnexion($nomEmp,$mdp,$catEmp,$login);
                    ctlConnexionAutomatique($login,$mdp);
                } else {
                    afficherErreurConnexion();
                }
            } else {
                afficherErreurConnexion();
            }
        } else {
            afficherErreurConnexion();
        }
    }
    //AGENT

function ctlAffichageAgent(){
	affichageAgent();
}




//GESTION CLIENT
function ctlAffichageGestionCli(){
	$client=rechercherClient();
	affichageGestionCli($client);
}

//ajouter client
function ctlAjouterCli($nom,$prenom,$date,$adresse,$num,$mail,$montantMax){
		//if(sizeof($num)==9) {
			ajouterClient($nom,$prenom,$date,$adresse,$num,$mail,$montantMax);	
			ctlAffichageGestionCli();
		//} else {
		//	throw new Exception("Impossible d'ajouter le client");
		//}
}
//supprimer client
function ctlSupprimerClient(){
	foreach($_POST['check'] as $key => $idCli){
		supprimerClient($idCli);
	}
	ctlAffichageGestionCli();
}

//modifier client
	function ctlAffichageModifierClient($idClient){
		if (!empty($idClient)) {
			$ligne = rechercherUnClient($idClient);
			$nom=$ligne->nom;
			$prenom=$ligne->prenom;
			$date=$ligne->date;
			$adresse=$ligne->adresse;
			$num=$ligne->num;
			$mail=$ligne->mail;
			$montantMax=$ligne->montantMax;
			affichageModifCli($nom,$prenom,$date,$adresse,$num,$mail,$montantMax,$idClient);
		} else {
			throw new Exception("Un des champs est invalide");
		}
			
	}
	
	function ctlModifierClient($nom,$prenom,$date,$adresse,$num,$mail,$montantMax,$idClient) {
		if(!empty($nom)&&!empty($prenom)&&!empty($date)&&!empty($adresse)&&!empty($num)&&!empty($mail)&&!empty($montantMax)&&!empty($idClient)) {
			modifierClient($nom,$prenom,$date,$adresse,$num,$mail,$montantMax,$idClient);
			ctlAffichageGestionCli();
		}else{
			throw new Exception("Un des champs est invalide");
		}
			
	}
	
	
	
	//SYNTHESE CLIENT
	function ctlAffichageSyntheseCli(){
		$clients=rechercherClient();
		$client=1;
		$intervention=null;
		affichageSyntheseCli($clients,$client, $intervention);
	}
	
	function ctlAffichageRechercherSyntheseCli($idClient){
				if(!empty($idClient)){
			$client=rechercherUnClient($idClient);
			$intervention=rechercherInterventionEtMontant($idClient);
			$clients=rechercherClient();			
			affichageSyntheseCli($clients,$client,$intervention);
				}
	}
	
	function ctlAffichageRechercherIDCli($client){
	affichageRechercherIDCli($client);
	}

	function ctlAffichageRechercherCli($nom,$prenom,$date){
		if(!empty($nom)&&!empty($prenom)&&!empty($date)){
		$client=rechercherIDClient($nom,$prenom,$date);
		affichagerechercherIDCli($client);
			}

        } 

		
		
		
//GESTION FINANCIERE
function ctlAffichageGestionFin(){   //affichage avant saisie
		$clients=rechercherClient();
		$interventionsDifférées=null;
		$interventionAPayer=null;
		$montantMax=null;
		$client=null;
		affichageGestionFin($clients,$client,$interventionsDifférées,$interventionAPayer,$montantMax);
}
	
function ctlAffichageRechercherFinanceCli($idClient){ //affichage après saisie
		$clients=rechercherClient();
		$interventionsDifférées=rechercherinterventionsDifférées($idClient);
		$interventionAPayer=rechercherinterventionAPayer($idClient);
		$montantMax=getMontantMax($idClient);
		$client=rechercherUnClient($idClient);
		affichageGestionFin($clients,$client,$interventionsDifférées,$interventionAPayer,$montantMax);
}

function ctlPayementCaisse($idClient){ 

	foreach($_POST['check'] as $key => $code){
		$etat=etatIntervention($code);
		if($etat->etat=="différé"){
			$montantMax=getMontantMax($idClient); //recuperer le montantMax du client
       	    $montantMax=intval($montantMax->montantMax,10); //le convertir en int de base 10
            $prix=getMontantIntervention($code); //recuperer le montant de l'interv
            $prix=intval($prix->montant,10); // le convertir en int
            $montantAChanger=$prix+$montantMax; // faire la somme
           modifierMontantMax($montantAChanger,$idClient); //modifier le montantMax du client
		   
		}
		payementCaisse($code); //on change l'état de l'intervention correspondante en "payé"
		ctlAffichageGestionFin();
		
	}
	

}

function ctlPayementDifféré($idClient){ 
			foreach($_POST['check'] as $key => $code){
			//$code=intval($code,10);
			$montantMax=getMontantMax($idClient); //recuperer le montantMax du client
       	    $montantMax=intval($montantMax->montantMax,10); //le convertir en int de base 10
            $prix=getMontantIntervention($code); //recuperer le montant de l'interv
            $prix=intval($prix->montant,10); // le convertir en int
            $montantAChanger=$montantMax-$prix; // faire la différence	
           modifierMontantMax($montantAChanger,$idClient); //modifier le montantMax du client
		   
			}
		payementDifféré($code); //on change l'état de l'intervention correspondante en "payé"
		
		ctlAffichageGestionFin();
		
	
}
    //CONNEXION AUTOMATIQUE
    function ctlConnexionAutomatique($login,$mdp) {
        
        $ligne = employePresent2($login,$mdp);
        $catEmp=$ligne->categorie;
        if($catEmp == "Directeur") {
            ctlAffichageDirecteur();
        
        } elseif ($catEmp == "Agent") {
            ctlAffichageAgent();
        } elseif ($catEmp == "Mécanicien") {
            //ctlAffichageMecanicien();
        } else {
            throw new Exception("Voir ctlConnexionAutomatique");
        }
        
    }
   
    function ctlInsertionConnexion($nomEmp,$mdp,$catEmp,$login) {
        insertionConnexion($nomEmp,$mdp,$catEmp,$login);
    } 
    function ctlEmployePresent() {
        $res=employePresent();
        if($res==false){
            return 1;
        }else {
            return $res;
        }
    }
	function ctlDeconnexion(){
        deconnexion();
    }
    function ctlAffichageConnexion(){
        affichageConnexion();
    }

    function ctlAffPlanningAgent($nomMeca){
        $today=date("Y-m-d");
        $Meca=trouverEmpNom($nomMeca);
        $user=verificationUser();
        $listeMecas=nomMecas();
        $nextmonday=date('Y-m-d', strtotime("next Monday ".$today));
        $thismonday=date('Y-m-d', strtotime("last Monday ".$nextmonday));
        $interventions=lesIntersMeca($Meca,$thismonday,$nextmonday);
        $formations=lesFormMeca($Meca,$thismonday,$nextmonday);
        affPlanningAgent($listeMecas,$thismonday,$user,$Meca,$interventions,$formations);
    }

    function ctlAffMecas(){
        $listeMecas=nomMecas();
        affLDMecas($listeMecas);
    }
		
function ctlAffAjouterIntervention($heure_rdv,$date_rdv,$meca_rdv) {
        $user=verificationUser();
        $listeTypeInter=lesInterventions();
        $listeClient=getListeClients();
        affAjouterIntervention($date_rdv,$heure_rdv,$meca_rdv,$user,$listeTypeInter,$listeClient);
    }

function ctlAjouterIntervention($date_rdv,$heure_rdv,$meca_rdv,$clientID,$typeInter) {
        ajouterIntervention($date_rdv,$heure_rdv,$meca_rdv,$clientID,$typeInter);
        $inter=trouverInter($typeInter);
        ctlAffPlanningAgent($meca_rdv);
    }

function ctlVoirIntervention_agent($nomClient,$prenomClient,$numClient,$mailClient,$type_intervention,$pieces,$date,$heure,$meca) {
        voirIntervention_agent($nomClient,$prenomClient,$numClient,$mailClient,$type_intervention,$pieces,$date,$heure,$meca);
    }



//affPlanningAgent($listeMecas,$thismonday,$user,$Meca,$interventions,$formations)


    function ctlPlanNextWeek_agent($lundi,$nomMeca) {
        $user=verificationUser();
        $Meca=trouverEmpNom($nomMeca);
        $liste=nomMecas();
        $thismonday=semaineProchaine($lundi);
        $nextmonday=semaineProchaine($thismonday);
        $interventions=lesIntersMeca($Meca,$thismonday,$nextmonday);
        $formations=lesFormMeca($Meca,$thismonday,$nextmonday);
        affPlanningAgent($liste,$thismonday,$user,$Meca,$interventions,$formations);
    }

function ctlPlanPreviousWeek_agent($lundi,$nomMeca) {
        $user=verificationUser();
        $Meca=trouverEmpNom($nomMeca);
        $liste=nomMecas();
        $nextmonday=$lundi;
        $thismonday=date('Y-m-d', strtotime("last Monday ".$nextmonday));
        $interventions=lesIntersMeca($Meca,$thismonday,$nextmonday);
        $formations=lesFormMeca($Meca,$thismonday,$nextmonday);
        affPlanningAgent($liste,$thismonday,$user,$Meca,$interventions,$formations);
    }

