<?php
//SITE
	try { 
		require_once('controleur/controleur.php');
		
        //CONNEXION
        
        if(isset($_POST['connexion'])) {
            $login=$_POST['login'];
            $mdp=$_POST['mdp'];
            ctlTantativeConnexion($login,$mdp);
        } elseif(isset($_POST['seDeconnecter'])) {
            ctlDeconnexion();
            affichageConnexion();
		//GESTION EMPLOYE   
        } elseif (isset($_POST['employe'])) {   
			ctlAffichageEmploye();
	
            //SUPPRIMER EMPLOYE
		} elseif (isset($_POST['supprimerEmploye']) && isset($_POST['check'])){ 
				ctlSupprimerEmploye();		
		
            //CREER EMPLOYE
		} elseif (isset($_POST['creerEmp'])){
				$nomEmp=$_POST['nomEmp'];
				$loginEmp=$_POST['loginEmp'];
				$mdpEmp=$_POST['mdpEmp'];
				$catEmp=$_POST['catEmp'];
				ctlCreerEmploye($nomEmp,$loginEmp, $mdpEmp, $catEmp); 				
			//MODIFIER EMPLOYE
		} elseif (isset($_POST['modifEmp'])){ 
				$nomEmp=$_POST['modifierS'];
			//MODIFIER EMPLOYE
                ctlAffichageModifierEmploye($nomEmp);
		} elseif (isset($_POST['modifierM'])){
				$login=$_POST['loginEmp'];
				$mdpEmp=$_POST['mdpEmp'];
				$catEmp=$_POST['catEmp'];
				$nomEmp=$_POST['nomEmp'];
				ctlModifierEmploye($login, $mdpEmp, $catEmp, $nomEmp);
				
				
		//GESTION INTERVENTION
		} elseif (isset($_POST['intervention'])) { 
				ctlAffichageIntervention();
         
        //GESTION INTERVENTION  
            //CREER INTERVENTION
		} elseif (isset($_POST['creerIntervention'])){ 
                $nomTI=$_POST['nomTI'];
                $montant=$_POST['montant'];
                $listePiece=$_POST['listePiece'];
				ctlCreerIntervention($nomTI,$montant,$listePiece);
            //SUPPRIMER INTERVENTION
		} elseif (isset($_POST['supprimerIntervention'])){ 
				ctlSupprimerIntervention();    
            //MODIFIER INTERVENTION
		} elseif (isset($_POST['modifTI'])){ 
                $nomTI=$_POST['nomTI'];
            //MODIFIER INTERVENTION
				ctlAffichageModifierIntervention($nomTI);
        } elseif (isset($_POST['modifierTI'])) {
            $montant=$_POST['montant'];
            $listePiece=$_POST['listePiece'];
            $nomTI=$_POST['nomTI'];
            ctlModifierIntervention($nomTI,$montant,$listePiece);
            
                //GESTION CLIENT	
            //Afficher la page de gestion de clients
        } elseif (isset($_POST['gestionCli'])){ 
            ctlAffichageGestionCli();
        

            //Ajouter client
        }elseif (isset($_POST['ajouterCli'])){ 
            $nom=$_POST['nom'];
            $prenom=$_POST['prenom'];
            $date=$_POST['date'];
            $adresse=$_POST['adresse'];
            $num=$_POST['num'];
            $mail=$_POST['mail'];
            $montantMax=$_POST['montantMax'];
            ctlAjouterCli($nom,$prenom,$date,$adresse,$num,$mail,$montantMax);
  
        //supprimer client
        }elseif (isset($_POST['supprimer'])&& !empty($_POST['check'])){
            ctlSupprimerClient();
        

        //modifier client
        }elseif (isset($_POST['modifierCli'])){ 
                $idClient=$_POST['modifier'];
                ctlAffichageModifierClient($idClient);
        
        }elseif(isset($_POST['modifierClient'])){ 
                $nom=$_POST['nom'];
                $prenom=$_POST['prenom'];
                $date=$_POST['date'];
                $adresse=$_POST['adresse'];
                $num=$_POST['num'];
                $mail=$_POST['mail'];
                $montantMax=$_POST['montantMax'];
                $idClient=$_POST['idClient'];		
                ctlModifierClient($nom,$prenom,$date,$adresse,$num,$mail,$montantMax,$idClient);
        
                //SYNTHESE CLIENT
        }elseif (isset($_POST['syntheseCli'])){ 
            ctlAffichageSyntheseCli();
        

        }elseif (isset($_POST['rechercherSyntheseCli'])){
            $idClient=$_POST['idClient'];
            ctlAffichageRechercherSyntheseCli($idClient);
        


        //RECHERCHE ID CLIENT
        }elseif (isset($_POST['rechercherClient'])){
            $nom=$_POST['nom'];
            $prenom=$_POST['prenom'];
            $date=$_POST['date'];
            ctlAffichageRechercherCli($nom,$prenom,$date);
        

        }elseif (isset($_POST['rechercherIDClient'])){
                $client=1;
                ctlAffichageRechercherIDCli($client);
        

        //GESTION FINANCIERE
        }elseif (isset($_POST['gestionFin'])){ 
            ctlAffichageGestionFin();
        
        }elseif (isset($_POST['rechercherFinanceCli'])){   //sélection du client dont il faut faire les finances
            $idClient=$_POST['idClient'];
            ctlAffichageRechercherFinanceCli($idClient);
         
        }elseif(!empty($_POST['check']) && isset($_POST['rembourser'])){ //fieldset payements avancés en différé
                $idClient=$_POST['idClient'];
                ctlPayementCaisse($idClient);
        


        }elseif(!empty($_POST['check']) && isset($_POST['payementDifféré'])){ //fieldset payements en attente > payer en différé 
                $idClient=$_POST['idClient'];
                ctlPayementDifféré($idClient);
        }elseif(!empty($_POST['check']) && isset($_POST['payer'])){ //fieldset payements en attente > payer en cash
                $idClient=$_POST['idClient'];
                ctlPayementCaisse($idClient);
        
            
            
            
        //juliette
        }else if(isset($_POST['gestionRdv'])){
            ctlAffMecas();
        }else if(isset($_POST['voir_plan_agent'])){
            $nmMeca=$_POST['nomMeca'];
            ctlAffPlanningAgent($nmMeca);
        }else if(isset($_POST['prendre_rdv'])){
            $heure_rdv=$_POST['heure_rdv'];
            $date_rdv=$_POST['date_rdv'];
            $meca_rdv=$_POST['nomMeca_rdv'];
            ctlAffAjouterIntervention($heure_rdv,$date_rdv,$meca_rdv);
        } else if (isset($_POST['prendre_rdv1'])) {
            $clientID=$_POST['clientID'];
            $typeInter=$_POST['listeTypeInter'];
            $heure_rdv=$_POST['heure_rdv'];
            $date_rdv=$_POST['date_rdv'];
            $meca_rdv=$_POST['mecas'];
            ctlAjouterIntervention($date_rdv,$heure_rdv,$meca_rdv,$clientID,$typeInter);
        }else if (isset($_POST['voir_intervention_agent'])){
            $meca=$_POST['mecas'];
            $nomClient=$_POST['nomClient'];
            $prenomClient=$_POST['prenomClient'];
            $numClient=$_POST['numClient'];
            $mailClient=$_POST['mail'];
            $type_intervention=$_POST['typeIntervention'];
            $pieces=$_POST['pieces'];
            $date=$_POST['date'];
            $heure=$_POST['heure']; 
            ctlVoirIntervention_agent($nomClient,$prenomClient,$numClient,$mailClient,$type_intervention,$pieces,$date,$heure,$meca);
        }else if (isset($_POST['semaine_pro_agent'])) {
            $lundi=$_POST['lundi'];
            $nomMeca=$_POST['Mecanicien'];
            ctlPlanNextWeek_agent($lundi,$nomMeca);
        }else if (isset($_POST['semaine_pre_agent'])) {
            $lundi=$_POST['lundi'];
            $nomMeca=$_POST['Mecanicien'];
            ctlPlanPreviousWeek_agent($lundi,$nomMeca); 
            
            
            
            
            
            
            
            
            
            
            } else {
            //CONNEXION
                $res=ctlEmployePresent();
                if($res==1) {
                    ctlAffichageConnexion();
                } else {
                    $login=$res[0]->login;
                    $mdp=$res[0]->mdp;
                    ctlConnexionAutomatique($login,$mdp);
                }

            } 
        
		
	} catch(Exception $e) {
		$msg = $e->getMessage();
		ctlErreur($msg);
	}		
	